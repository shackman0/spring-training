this software belongs to Floyd Shackelford. 
you are allowed to scrutinize this software for the purposes of evaluating my skills as a software engineer. 
any other use requires advance permission.

i call this "spring training" because this is the work i have done in my efforts to self-teach myself Spring. 
(I am very autodidactic. in all of my career, the best software engineers were self-taught.)
i can't just read a book about a new technology, i need to actually "chase a ball" to learn it. here are some
of my artifacts from chasing the ball.